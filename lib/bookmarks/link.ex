defmodule Bookmarks.Link do
  alias Bookmarks.Link

  defstruct date: 0, href: "", icon: "", label: "", tags: [ ]

  @type t :: %Link{
    date: non_neg_integer,
    href: String.t,
    icon: String.t,
    label: String.t,
    tags: String.t
  }
end
