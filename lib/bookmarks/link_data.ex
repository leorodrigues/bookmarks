defmodule Bookmarks.LinkData do
  use Ecto.Schema
  import Ecto.Changeset

  schema "links" do
    field :date, :integer
    field :href, :string
    field :icon, :string
    field :label, :string

    timestamps()
  end

  @doc false
  def changeset(link_data, attrs) do
    link_data
      |> cast(attrs, [:href, :date, :icon, :label])
      |> validate_required([:href, :date, :label])
  end
end
