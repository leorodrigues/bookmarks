defmodule Bookmarks.NetscapeFile.Parsing do
  @moduledoc """
  Implements the conversion of Netscape bookmark documents into links.
  """

  alias Bookmarks.Link

  @link_content_regex ~r{^.*HREF="([^"]*)"( ADD_DATE="([^"]*)")?( ICON="([^"]*)")?>(.*)</A>}
  @tag_content_regex ~r{^.*<H3.*>(.*)</H3>}
  @section_start_regex ~r/^<DT><H3.*/
  @section_end_regex ~r[^</DL>.*]
  @link_start_regex ~r/<DT><A.*/
  @new_line_regex ~r/\r?\n/

  @typedoc """
  The source of character strings.
  """
  @type chunks :: Enumerable.t()

  @typedoc """
  A sequence of lines.
  """
  @type lines :: Enumerable.t()

  @doc """
  Takes in a sequence (possibly infinite) of character chunks and outputs
  a sequence of strings breaking at new line characters.
  """
  @spec stream_out_lines(chunks) :: Enumerable.t()
  def stream_out_lines(chunks) do
    chunks
      |> Stream.chunk_while("", &add_to_line/2, &finish_line/1)
      |> Stream.flat_map(fn i -> Enum.map(i, &wrap_line/1) end)
  end

  @doc """
  Takes in a sequence (possibly infinite) of strings representing single
  text lines and parses them into a sequence of links.
  """
  @spec stream_out_links(lines) :: Enumerable.t()
  def stream_out_links(lines) do
    lines
      |> Stream.map(&try_unwrapping_line/1)
      |> Stream.map(&String.trim/1)
      |> Stream.with_index(1)
      |> Stream.transform({ [ ] }, &text_line_to_link/2)
  end

  defp add_to_line(chunk, reminder) do
    [ last | front ] = Enum.reverse(String.split(chunk, @new_line_regex))
    emit_lines(reminder, Enum.reverse(front), last)
  end

  defp finish_line(reminder) do
    emit_lines("", [reminder], nil)
  end

  defp emit_lines(_, [ ], reminder) do
    { :cont, reminder }
  end

  defp emit_lines(first, lines, reminder) do
    [ head | middle ] = lines
    { :cont, [ first <> head | middle ], reminder }
  end

  defp wrap_line(text_line), do: { :ok, text_line }

  defp try_unwrapping_line({ :ok, line }), do: line

  defp try_unwrapping_line(line), do: line

  defp text_line_to_link({ line, number }, state) do
    cond do
      line =~ @section_start_regex -> enter_section(line, state)
      line =~ @link_start_regex -> make_section_content(line, number, state)
      line =~ @section_end_regex -> exit_section(state)
      true -> skip_line(line, number, state)
    end
  end

  defp enter_section(line, state), do: { [ ], push_tag(line, state) }

  defp exit_section(state), do: { [ ], pop_tag(state) }

  defp make_section_content(line, number, state) do
    case Regex.run(@link_content_regex, line) do
      nil -> skip_line(line, number, state)
      _ = result -> { [emit_link(result, number, state)], state }
    end
  end

  defp skip_line(line, number, state) do
    { [emit_skipped(line, number)], state }
  end

  defp push_tag(line, { tags }) do
    case Regex.run(@tag_content_regex, line) do
      [ _, tag ] -> { [ tag | tags ] }
      nil -> { [ ] }
    end
  end

  defp emit_link([ _, href, _, date, _, icon, label ], _, { tags }) do
    { :ok, make_link(href, label, integer_date(date), icon, tags) }
  end

  defp emit_skipped(line, number), do: { :skipped, number, line }

  defp pop_tag({ tags }) do
    case tags do
      [ _ | t ] -> { t }
      [ ] -> { [ ] }
    end
  end

  defp make_link(href, label, date, icon, tags) do
    %Link{ tags: tags, date: date, href: href, icon: icon, label: label }
  end

  defp integer_date(date), do: String.to_integer(date)
end
