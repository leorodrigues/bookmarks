defmodule BookmarksWeb.NetscapeFileController do
  use BookmarksWeb, :controller

  alias Bookmarks.NetscapeFile.Parsing

  def load(conn, _params) do
    start = fn -> conn end
    finish = fn conn -> conn end
    spy = fn i -> IO.inspect i; i end

    items = Stream.resource(start, &next_chunk/1, finish)
      |> Parsing.stream_out_lines
      |> Parsing.stream_out_links
      |> Stream.run

    json(conn, %{ message: "done" })
  end

  defp bare_results({ :skipped, _, _ }), do: false

  defp bare_results(_), do: true

  defp next_chunk(conn) do
    case Plug.Conn.read_body(conn, [length: 128, read_length: 128]) do
      { :ok, "", conn } -> { :halt, conn }
      { :ok, chunk, conn } -> { [chunk], conn }
      { :more, chunk, conn } -> { [chunk], conn }
    end
  end
end
