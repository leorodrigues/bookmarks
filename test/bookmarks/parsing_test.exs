defmodule Bookmarks.NetscapeFile.ParsingTest do
  use ExUnit.Case

  alias Bookmarks.NetscapeFile.Parsing

  test "stream_out_lines/1 should handle a single chunk" do
    assert [{:ok, "test"}] = ["test"]
      |> Parsing.stream_out_lines
      |> Enum.to_list
  end

  test "stream_out_lines/1 should handle a multi line chunk" do
    assert [{:ok, "one"}, {:ok, "two"}] = ["one\ntwo"]
      |> Parsing.stream_out_lines
      |> Enum.to_list
  end

  test "stream_out_lines/1 should preserve empty lines chunk" do
    assert [{:ok, "one"}, {:ok, ""}, {:ok, "two"}] = ["one\n\ntwo"]
      |> Parsing.stream_out_lines
      |> Enum.to_list
  end

  test "stream_out_lines/1 should join reminders" do
    expected_result = [{:ok, "one two"}, {:ok, "three"}, {:ok, "four"}]

    assert ^expected_result = ["one", " two\nthree\n", "four"]
      |> Parsing.stream_out_lines
      |> Enum.to_list
  end
end
