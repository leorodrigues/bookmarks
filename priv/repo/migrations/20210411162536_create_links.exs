defmodule Bookmarks.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links) do
      add :href, :string
      add :date, :integer
      add :icon, :string
      add :label, :string

      timestamps()
    end

  end
end
